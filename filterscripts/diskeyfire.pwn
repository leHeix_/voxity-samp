/*
	Disable Vehicle Fire Sync
	AdrianGraber
*/

#define FILTERSCRIPT

#include <a_samp>
#include <Pawn.RakNet>

const VEHICLE_SYNC = 200;
new CallbackTime[MAX_PLAYERS];
static const DetectedModels[] =
{
	//Add here ModelIDs of vehicles you want to disallow firing from vehicle
	//In this examlpe I will add Hunter, Sea Sparrow and Rhino
	//Try not to use this in normal vehicles, as it will prevent nitro syncing
	425,
	432,
	447
};

public OnFilterScriptInit()
{
	print("\n--------------------------------------");
	print("   Disallow Vehicle Fire Started...    ");
	print("--------------------------------------\n");
	return 1;
}

public OnFilterScriptExit()
{
	return 1;
}

IPacket:VEHICLE_SYNC(playerid, BitStream:bs)
{

    new inCarData[PR_InCarSync];

    BS_IgnoreBits(bs, 8);
    BS_ReadInCarSync(bs, inCarData);
    
    new modelid = GetVehicleModel(inCarData[PR_vehicleId]);

	//RIDE2DAY
    for(new i; i != sizeof(DetectedModels); i++)
	{
	    if(modelid == DetectedModels[i])
	    {
	        break;
	    }

	    if(sizeof(DetectedModels) - i == 1)
	    {
	        return 1;
	    }
	}
	
	if((inCarData[PR_keys] & KEY_FIRE) || (inCarData[PR_keys] & KEY_ACTION))
	{
		inCarData[PR_keys] &= ~KEY_FIRE;
    	inCarData[PR_keys] &= ~KEY_ACTION;

        BS_SetWriteOffset(bs, 8);
    	BS_WriteInCarSync(bs, inCarData);
    	
		if(CallbackTime[playerid] < gettime())
		{
		    CallLocalFunction("OnPlayerFiresForbiddenVeh", "d", playerid);
		    CallbackTime[playerid] = gettime() + 5;
		}

	}

    return 1;
}

forward OnPlayerFiresForbiddenVeh(playerid);
public OnPlayerFiresForbiddenVeh(playerid)
{
	SendClientMessage(playerid, -1, "You can't fire with this vehicle! Your action will not be synced!"); //Example, better to do this with a Dialog or something to scare the player
	return 1;
}