# Voxity Roleplay
GameMode utilizada en el servidor Voxity Roleplay. Basada en la GM [Super Roleplay](https://github.com/adri1samp/Super-RolePlay) de 
[adri1](https://github.com/adri1samp)  
[DISCORD](https://discord.gg/fR3Xc5X)

# Diferencias entre GM SRP y GM VX:RP
- Se implementaron las misiones (que se encuentran en progreso)
- Se agregaron facciones administrables con /fac
- Se cambiaron la forma de entrar y salir de los interiores, además de cambiar de comando por tecla el abrir tu vehículo.
- Se agrego un concesionario en el aeropuerto para aeronaves.
- Se agregaron los niveles de VIP, con nombres, precio y beneficios cambiables según el scripter.
- Se agrego el sistema de muebles en las casas
- Se agrego el comando /bonus
- Se centró más el servidor en Los Santos, cambiando así la localización del trabajo de Camioneros y posibilitando a los usuarios de plantar como agricultores donde quieran.
- Se agrego una villa cerca de Unity, donde los usuarios pueden comprar armas más caras que en el Mercado Negro (kjj), además de semillas ilegales para plantar.
- Se agrego un sistema de cargos parecido al de FenixZone, obviamente no es igual, pero por ahí va la idea.
- Se cambio la ubicación de spawn.
- Se agrego una misión de narcotráfico, además de mapear una isla exclusiva de Voxity (gracias yuta)
- Se modificaron los recorridos de camionero.
- Se modificaron los Pay 'n Spray de tal forma de que solo se habiliten cuando no haya mecánicos en servicio.


# Instalación
- Crear base de datos SQLite en `/scriptfiles/SERVER` llamada Voxity.db (default) con la estructura de `Voxity.sql`
- (OPCIONAL) Cambiar nombre de la base de datos en `/gamemodes/voxity.pwn`:  
Línea 21404, función ConnectDatabase
```pawn
ConnectDatabase()
{
	Database = db_open("SERVER/Voxity.db");
	if(Database == DB:0)
	{
		print("\n----------------------------------");
		print("  ¡ATENCIÓN! No hay conexión con la base de datos.\n");
		print("----------------------------------\n");
		SendRconCommand("exit");
	}
	else
	{
		print("La conexión con la base de datos funciona.");
		db_query(Database, 
		"\
			PRAGMA FOREIGN_KEYS = ON;\
			UPDATE `CUENTA` SET `CONNECTED` = '0', PLAYERID = '-1';\
		");
	}
	return 1;
}
```
Cambiar `SERVER/Voxity.db` por `SERVER/NOMBREDB.db`  
- Cambiar configuración por defecto:  
Línea 36 .. 45
```pawn
#define MAX_PLAYERS 75

/* Config */
#define SERVER_NAME			"Voxity Roleplay"
#define SERVER_SHORT_NAME	"Voxity"
#define SERVER_HOSTNAME 	" « Voxity Roleplay | APERTURA 1/1/2020 »"
#define SERVER_GAMEMODE		"Voxity "SERVER_VERSION"-"BUILD_VERSION""
#define SERVER_LANGUAGE		"Español / Spanish / Espanol"
#define SERVER_WEBSITE		"discord.gg/fR3Xc5X"
#define SERVER_COIN			"Voxicoins"
```

- Compilar GM y montar
- profit
